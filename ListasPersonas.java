/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases23;

import java.util.ArrayList;

/**
 *
 * @author andre
 */
public class ListasPersonas {
     public double promedioEdad(Personas[] p){
       double sumEdad=0;
       
       for(Personas r:p){
           sumEdad=sumEdad+r.getEdad();
       }
       //equivalente for
//       Personas obj;
//       for (int i=0; i<p.length;i++){
//           obj= p[i];
//           sumEdad=sumEdad+obj.getEdad();
//       }
       double resultado=sumEdad/p.length;
      return  resultado;
    }
    
//    public double edadMax(ArrayList<Personas> p){
//        double max=p.get(0).getEdad();
//        for(int i=1;i<p.size();i++){
//            if(p.get(i).getEdad()>max){
//                max=p.get(i).getEdad();
//                
//                                                                                                                                                              
//            }
//        }
//        return max;
//    }
    public Personas edadMaxPersona(ArrayList<Personas> p ){
        Personas max= new Personas();
              max=  p.get(0);
//        Personas min= new Personas();
//              min=  p.get(0);
        for(int i=1;i<p.size();i++){
            if(p.get(i).getEdad()>max.getEdad()){
                    max=p.get(i);
            }
        }
//        for(int i=1;i<p.size();i++){
//            if(p.get(i).getEdad()<min.getEdad()){
//                    min=p.get(i);
//            }
//        }
        return max;

    }
    public Personas edadMinPersona(ArrayList<Personas> p ){
        Personas min= new Personas();
              min=  p.get(0);
        for(int i=1;i<p.size();i++){
            if(p.get(i).getEdad()<min.getEdad()){
                    min=p.get(i);
            }
        }
        return min;
    }    
}

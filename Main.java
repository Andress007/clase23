/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases23;

import java.util.ArrayList;

/**
 *
 * @author andre
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Personas p1=new Personas();
        p1.setNombre("juan");
        p1.setEdad(50);
        
        Personas p2=new Personas();
        p2.setNombre("Luis");
        p2.setEdad(100);
        
        Personas p3=new Personas();
        p3.setNombre("Carlos");
        p3.setEdad(35);
        
        Personas p4=new Personas();
        p4.setNombre("Maria");
        p4.setEdad(60);
        
        ArrayList <Personas> nombres=new ArrayList<Personas>();
        nombres.add(p1);
        nombres.add(p2);
        nombres.add(p3);
        nombres.add(p4);
        ListasPersonas listas=new ListasPersonas();
//        double maxP=listas.edadMax(nombres);
        
//        System.out.println(""+maxP);
        System.out.println("El de mayor edad es: "+listas.edadMaxPersona(nombres).toString());
        System.out.println("El de menor edad es: "+listas.edadMinPersona(nombres).toString());
    }
    
}
